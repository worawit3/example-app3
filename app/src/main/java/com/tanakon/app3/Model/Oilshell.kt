package com.tanakon.app3.Model

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.StringRes

data class Oilshell(
    @StringRes val stringResourcePrice: Int,
    @StringRes val stringResourceOil: Int,
)