package com.tanakon.app3.data

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.tanakon.app3.Model.Oilshell
import com.tanakon.app3.R

class Data {
    fun loadOilShell(): List<Oilshell> {
        return listOf<Oilshell>(
            Oilshell(R.string.price1,R.string.oil1),
            Oilshell(R.string.price2,R.string.oil2),
            Oilshell(R.string.price3,R.string.oil3),
            Oilshell(R.string.price4,R.string.oil4),
            Oilshell(R.string.price5,R.string.oil5),
            Oilshell(R.string.price6,R.string.oil6),
            Oilshell(R.string.price7,R.string.oil7),
            Oilshell(R.string.price8,R.string.oil8),
            Oilshell(R.string.price9,R.string.oil9),
            )
    }
}